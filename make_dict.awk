#!/usr/bin/awk -f
BEGIN{
    FS = " ";
    OFS = "\t";
    ORS = "\n";
}{
    if (NR == FNR){
        d[$1] = $3;
    }else{
        if (length($2) < 8){
            if (d[$2] > 0){
                print $2, $1, d[$2];
            }
        }
    }
}END{
}
